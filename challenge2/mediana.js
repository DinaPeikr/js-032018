document.addEventListener("DOMContentLoaded", (event) => {

    const calcButton = document.querySelector('.btn-calc');

    calcButton.onclick = () => {
        let numbersData = getNumbers('#numbers');
        let numbersArray = dataToArray(numbersData);
        //sortArray(numbersArray);
        bubbleSort(numbersArray);
        let medianNumber = getMedian(numbersArray);
        displayMedian(numbersArray, medianNumber, '.messageBox');
    }

    function displayMedian(arrayName, medianInt, output) {
        const messageBox = document.querySelector(output);
        let arrayString = arrayName.join(', ');
        let messageText = `Ваш массив: ${arrayString} <br>
                           Медиана в этом массиве чисел: 
                           является ${medianInt} `;
        messageBox.classList.add('show');
        messageBox.innerHTML = messageText;

    }

 });

function getNumbers(selector) {
    const numbers = document.querySelector(selector);
    let numbersData = numbers.value;
    return numbersData;
}

function dataToArray(data) {
    let numbersArray = data.split(',');
    return numbersArray;
}

function sortArray(array) {
    //Сортировка выбором
    for (let i = 0; i < array.length - 1; i++) {
        let min = i;
        for (let j = i + 1; j < array.length; j++) {

            if (parseInt(array[j]) < parseInt(array[min])) {
                min = j;
            }

        }
        let tmp = array[min];
        array[min] = array[i];
        array[i] = tmp;
    }
    return array;
}

function bubbleSort(array) {
    //Сортировка пузырьком                         
    for (let i = 0; i < array.length - 1; i++) {
        for (let j = 0; j < array.length - 1 - i; j++) {

            if (parseInt(array[j + 1]) < parseInt(array[j])) {
                let tmp = array[j + 1];
                array[j + 1] = array[j];
                array[j] = tmp;
            }

        }
    }
    return array;
}

function getMedian(numbers) {
    let median = 0;
    let index1 = Math.floor(numbers.length / 2 - 1);
    let index2 = Math.floor(numbers.length / 2);

    if (numbers.length % 2 === 0) {
        median = (parseInt(numbers[index1]) + parseInt(numbers[index2])) / 2;
    } else {
        median = parseInt(numbers[index2]);
    }

    return median;
}