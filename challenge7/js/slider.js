const slider = $('#slider');
const slides = $('#slider img');

const sliderState = {
    activeSlide: 0,
    allSlides: {},
    slidesCount: 0
};

$(document).ready(() => {
    sliderInit();
});

const sliderInit = () => {
    let prevButton = document.createElement('button');
    prevButton.className = 'btn prev';
    prevButton.innerHTML = '<<';
    slider.append(prevButton);

    let nextButton = document.createElement('button');
    nextButton.className = 'btn next';
    nextButton.innerHTML = '>>';
    slider.append(nextButton);

    sliderState.slidesCount = slides.length;
    sliderState.allSlides = slides.map((index, element) => {
        return {
            id: index,
            tag: $(element)
        }
    });

    displayActiveSlide(sliderState.allSlides, sliderState.activeSlide);
    $(prevButton).on('click', clickPrevButton);
    $(nextButton).on('click', clickNextButton);
};

const displayActiveSlide = (sliders, numberActiveSlider) => {
    for (let i = 0; i < sliders.length; i++) {

        if (i === numberActiveSlider) {
            sliders[i].tag.fadeIn(300);
        } else {
            sliders[i].tag.hide();
        }
    }

    return numberActiveSlider;
};

const clickPrevButton = () => {
    sliderState.activeSlide = prevSlideFunc(sliderState.activeSlide, sliderState.slidesCount);
    displayActiveSlide(sliderState.allSlides, sliderState.activeSlide);
};

const clickNextButton = () => {
    sliderState.activeSlide = nextSlideFunc(sliderState.activeSlide, sliderState.slidesCount);
    displayActiveSlide(sliderState.allSlides, sliderState.activeSlide);
};

const nextSlideFunc = (currentSlide, maxSlide) => {
    currentSlide++;

    if (currentSlide >= maxSlide) {
        currentSlide = 0;
    }

    return currentSlide;
};

const prevSlideFunc = (currentSlide, maxSlide) => {
    currentSlide--;

    if (currentSlide < 0) {
        currentSlide = maxSlide - 1;
    }

    return currentSlide;
};