document.addEventListener("DOMContentLoaded", (event) => {

    let counter1 = new Counter({
        counterButtons: ".counter1 .btn",
        outputBox: ".counter1 .outputBox"
    });

    let counter2 = new Counter({
        counterButtons: ".counter2 .btn",
        outputBox: ".counter2 .outputBox"
    });

    window.addEventListener("keydown", (e) => {
        let arrows = counter1.arrowsHandlerLeftRight(e);
        counter1.outputCounter(counter1.outputBox, arrows);
    });

    window.addEventListener("keydown", (e) => {
        let arrows = counter2.arrowsHandlerTopDown(e);
        counter2.outputCounter(counter2.outputBox, arrows);
    });
    
});

function Counter(object) {
    this.counterButtons = document.querySelectorAll(object.counterButtons);
    this.outputBox = document.querySelector(object.outputBox);
    let count = 0;

    for (let i = 0; i < this.counterButtons.length; i++) {
        this.counterButtons[i].addEventListener("click", (e) => {
            this.buttonsHandler(e);
            this.outputCounter(this.outputBox, count);
        });
    }

    this.buttonsHandler = (e) => {

        if (e.target.classList.contains('next')) {
            count++;

            if (count > 10) {
                count = 0;
            }

        } else if (e.target.classList.contains('prev')) {
            count--;

            if (count < 0) {
                count = 10;
            }

        }

        return count;
    }

    this.arrowsHandlerLeftRight = (e) => {

        if (e.keyCode === 39) {
            count++;

            if (count > 10) {
                count = 0;
            }

        } else if (e.keyCode === 37) {
            count--;

            if (count < 0) {
                count = 10;
            }

        }

        return count;
    }

    this.arrowsHandlerTopDown = (e) => {

        if (e.keyCode === 38) {
            count++;

            if (count > 10) {
                count = 0;
            }

        } else if (e.keyCode === 40) {
            count--;

            if (count < 0) {
                count = 10;
            }

        }

        return count;
    }

    this.outputCounter = (output, data) => {
        output.innerHTML = data;
    }

}