document.addEventListener("DOMContentLoaded", (event) => {

    let counter1 = new Counter({
        nextButton: ".counter1 .next",
        prevButton: ".counter1 .prev",
        outputBox: ".counter1 .outputBox",
        arrowsKeyDown: true
    });

    let counter2 = new Counter({
        nextButton: ".counter2 .next",
        prevButton: ".counter2 .prev",
        outputBox: ".counter2 .outputBox",
        arrowsKeyDown: false
    });

});

function Counter(object) {
    this.nextButton = document.querySelector(object.nextButton);
    this.prevButton = document.querySelector(object.prevButton);
    this.outputBox = document.querySelector(object.outputBox);
    this.arrowsKeyDown = object.arrowsKeyDown;
    let count = 0;

    this.increase = () => {
        count++;

        if (count > 10) {
            count = 0;
        }

        return count;
    }

    this.decrease = () => {
        count--;

        if (count < 0) {
            count = 10;
        }

        return count;
    }

    this.outputCounter = (output, data) => {
        output.innerHTML = data;
    }

    this.nextButton.addEventListener("click", () => {
        let counterIncrease = this.increase();
        this.outputCounter(this.outputBox, counterIncrease);
    });

    this.prevButton.addEventListener("click", () => {
        let counterDecrease = this.decrease();
        this.outputCounter(this.outputBox, counterDecrease);
    });

    if (this.arrowsKeyDown) {
        window.addEventListener("keydown", (e) => {

            if (e.keyCode === 39) {
                let counterIncrease = this.increase();
                this.outputCounter(this.outputBox, counterIncrease);
            } else if (e.keyCode === 37) {
                let counterDecrease = this.decrease();
                this.outputCounter(this.outputBox, counterDecrease);
            }

        });
    }
    
}