document.addEventListener("DOMContentLoaded", (event) => {

    //глобальные переменные для существующего счетчика
    const counterButtons = document.querySelectorAll('#counter > .btn');
    const outputBox = document.querySelector('#counter .outputBox');
    const addCounterButton = document.querySelector('.btn.add');

    //перебираем кнопки существующего счетчика
    for (let i = 0; i < counterButtons.length; i++) {
        counterButtons[i].addEventListener("click", (e) => {
            buttonsHandler(e);
            outputCounter(outputBox, count);
        });
    }

    // подключаем функцию для работы стрелок с клавиатуры для существующего счетчика
    window.addEventListener("keydown", (e) => {
        let arrows = arrowsHandler(e);
        outputCounter(outputBox, arrows);
    });

    //подключаем к кнопке функцию добавления созданного счетчика на экран
    addCounterButton.addEventListener('click', addCounter);

});

//глобальные переменные для добавляемых счетчиков
const startNumberOfCounters = 5;
let counters = {};
let count = 0;

//функция увеличения переменной count
increase = () => {
    count++;

    if (count > 10) {
        count = 0;
    }

    return count;
};

//функция уменьшения переменной count
decrease = () => {
    count--;

    if (count < 0) {
        count = 10;
    }

    return count;
};

//функция вывода на экран результата изменения переменной count
outputCounter = (output, data) => {
    output.innerHTML = data;
};

//функция создания объекта нового счетчика
createCounterObject = (numberOfCounters) => {
    for (let i = 1; i <= numberOfCounters; i++) {
        counters[i] = {
            id: 'counter' + i,
            increase: increase,
            decrease: decrease,
            outputCounter: outputCounter
        }
    }
};

//функция создания DOM элемента нового счетчика
createCounterElement = (countersArray) => {
    let counterItems = document.querySelector('.counters');
    for (let counter in countersArray) {
        let counterItem = document.createElement('div');
        counterItem.className = 'counter';
        counterItem.setAttribute('id', countersArray[counter].id);

        let prevButton = document.createElement('button');
        prevButton.className = 'btn prev';
        prevButton.innerHTML = 'Уменьшить -1';
        counterItem.appendChild(prevButton);

        let outputBox = document.createElement('div');
        outputBox.className = 'outputBox';
        outputBox.innerHTML = '0';
        counterItem.appendChild(outputBox);

        let nextButton = document.createElement('button');
        nextButton.className = 'btn next';
        nextButton.innerHTML = 'Увеличить +1';
        counterItem.appendChild(nextButton);

        counterItems.appendChild(counterItem);
        counters[counter].domElement = counterItem;

        nextButton.addEventListener("click", () => {
            let counterIncrease = increase();
            outputCounter(outputBox, counterIncrease);
        });

        prevButton.addEventListener("click", () => {
            let counterDecrease = decrease();
            outputCounter(outputBox, counterDecrease);
        });
    }

};

//функция добавления созданного счетчика на экран
addCounter = () => {
    createCounterObject(startNumberOfCounters);
    createCounterElement(counters);
};

//функция выбора кнопки для существующего счетчика
buttonsHandler = (e) => {

    if (e.target.classList.contains('next')) {
        count++;

        if (count > 10) {
            count = 0;
        }

    } else if (e.target.classList.contains('prev')) {
        count--;

        if (count < 0) {
            count = 10;
        }

    }

    return count;
};

//функция выбора клавишы на клавиатуре для существующего счетчика
arrowsHandler = (e) => {

    if (e.keyCode === 39) {
        count++;

        if (count > 10) {
            count = 0;
        }

    } else if (e.keyCode === 37) {
        count--;

        if (count < 0) {
            count = 10;
        }

    }

    return count;
};