document.addEventListener("DOMContentLoaded", (event) => {
    const rect = document.querySelectorAll('.rect');

    for (let i = 0; i < rect.length; i++) {
        rect[i].addEventListener('click', setRandomColor);
    }
    
});

getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

getRandomColor = () => {
    let r = getRandomInt(0, 255);
    let g = getRandomInt(0, 255);
    let b = getRandomInt(0, 255);
    return `rgb(${r}, ${g}, ${b})`;
}

setRandomColor = (e) => {
    e.target.style.backgroundColor = getRandomColor();

}