document.addEventListener("DOMContentLoaded", (event) => {

    const nextButton = document.querySelector('.btn.next');
    const prevButton = document.querySelector('.btn.prev');
    const outputBox = document.querySelector('.outputBox');

    nextButton.addEventListener("click", () => {
        let counterIncrease = increase();
        outputCounter(outputBox, counterIncrease);
    });

    prevButton.addEventListener("click", () => {
        let counterDecrease = decrease();
        outputCounter(outputBox, counterDecrease);
    });

});

let count = 0;

increase = () => {
    count++;
    return count;
}

decrease = () => {
    count--;

    if (count < 0) {
        count = 0;
    }

    return count;
}

outputCounter = (output, data) => {
    output.innerHTML = data;
}