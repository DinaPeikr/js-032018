document.addEventListener("DOMContentLoaded", (event) => {

    const counterButtons = document.querySelectorAll('.btn');
    const outputBox = document.querySelector('.outputBox');
    let count = 0;

    for (let i = 0; i < counterButtons.length; i++) {
        counterButtons[i].addEventListener("click", () => {

            if (counterButtons[i].classList.contains('next')) {
                count++;
            } else {
                count--;

                if (count < 0) {
                    count = 0;
                }

            }

            outputCounter(outputBox, count);
        });
    }
});

outputCounter = (output, data) => {
    output.innerHTML = data;
}