// Глобальные переменные
const addButton = document.querySelector('.btn.add');
const deleteButton = document.querySelector('.btn.del');
const addDivButton = document.querySelector('.btn.addDiv');
const startPeopleCount = 5;
let peoples = {};

// Функция генерации цвета
getRandomColor = () => {
    let letters = '0123456789ABCDEF';
    let randomColor = '#';

    for (let i = 0; i < 6; i++) {
        randomColor += letters[Math.floor(Math.random() * 16)];
    }

    return randomColor;
};

// Функция генерации имени
generateName = () => {
    let name = "";
    let possible = "01";

    for (let i = 0; i < 5; i++)
        name += possible.charAt(Math.floor(Math.random() * possible.length));

    return name;
};

// Создание массива человечков и сохранение их в глобальной переменной
createPeoples = (peopleCount) => {

    for (let i = 1; i <= peopleCount; i++) {
        peoples[i] = {
            id: i,
            name: generateName(),
            color: getRandomColor(),
            changePeopleColor: changePeopleColor,
            getDomPeopleById: getDomPeopleById,
            addPeopleInit: addPeopleInit
        };
    }

};

// Заселение человечков на страницу и сохранение их DOM элемента в переменной
// TODO: возможно стоит поменять название на более подходящее
createPeopleElement = (peoplesArray) => {
    let wrapperPeoples = document.getElementsByClassName('peoples-container')[0];

    for (let people in peoplesArray) {

        let peopleElement = document.createElement('i');
        peopleElement.className = 'fas fa-male';
        peopleElement.setAttribute('data-id', peoplesArray[people].id);
        peopleElement.setAttribute('data-name', peoplesArray[people].name);
        peopleElement.setAttribute('style', 'color: ' + peoplesArray[people].color);
        wrapperPeoples.appendChild(peopleElement);

        peoples[people].domElement = peopleElement;

        peoples[people].domElement.addEventListener('click', (e) => {
            clickedByPeople(e);
            console.log(getDomPeopleById(e));
        });

    }
};

// Изменение цвета человекчка
changePeopleColor = (e) => {
    e.target.style.color = getRandomColor();
};

getDomPeopleById = (e) => {
    return e.target.getAttribute('data-id');
};

// Функция, которая срабатывает при клике на человечка
clickedByPeople = (e) => {
    changePeopleColor(e);
};

// Функция, которая добавляет одного человечка
addPeopleInit = () => {
    let wrapperPeoples = document.getElementsByClassName('peoples-container')[0];
    let peopleElement = document.createElement('i');
    peopleElement.className = 'fas fa-male';
    peopleElement.setAttribute('data-id', wrapperPeoples.childNodes.length + 1);
    peopleElement.setAttribute('data-name', generateName());
    peopleElement.setAttribute('style', 'color: ' + getRandomColor());
    wrapperPeoples.appendChild(peopleElement);

    peopleElement.addEventListener('click', (e) => {
        clickedByPeople(e);
        console.log(getDomPeopleById(e));
    });

};

// Функция, которая удаляет одного человечка
deletePeopleInit = () => {
    let wrapperPeoples = document.getElementsByClassName('peoples-container')[0];
    let wrapperPeoplesChildNodes = wrapperPeoples.childNodes;
    wrapperPeoplesChildNodes[wrapperPeoples.childNodes.length - 1].remove();
};

// Функция, которая срабатывает при загрузке структуры документа (когда доступны
// DOM элементы
domContentLoaded = () => {
    createPeoples(startPeopleCount);
    createPeopleElement(peoples);
};

document.addEventListener('DOMContentLoaded', () => {

    domContentLoaded();

    addDivButton.addEventListener('click', () => {
        domContentLoaded();

    });

    addButton.addEventListener('click', () => {
        addPeopleInit();

    });

    deleteButton.addEventListener('click', () => {
        deletePeopleInit();

    });

});