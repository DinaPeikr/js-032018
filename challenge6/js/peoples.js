// Глобальные переменные
const addDivButton = $('.btn.addDiv');
let wrapperPeoples = $('.peoples-container');
let startPeopleCount = 5;
let CountOfPeople = startPeopleCount;
let peoples = {};

// Функция генерации цвета
let getRandomColor = () => {
    let letters = '0123456789ABCDEF';
    let randomColor = '#';

    for (let i = 0; i < 6; i++) {
        randomColor += letters[Math.floor(Math.random() * 16)];
    }

    return randomColor;
};

// Функция генерации имени
let generateName = () => {
    let name = "";
    let possible = "01";

    for (let i = 0; i < 5; i++)
        name += possible.charAt(Math.floor(Math.random() * possible.length));

    return name;
};

//Создание массива человечков и сохранение их в глобальной переменной
let createPeoples = (peopleCount) => {

    for (let i = 1; i <= peopleCount; i++) {
        peoples[i] = {
            id: i,
            name: generateName(),
            color: getRandomColor()
        };
    }
};

// Заселение человечков на страницу и сохранение их DOM элемента в переменной
let createPeopleElement = (peoplesArray) => {

    let peoplesDiv = document.createElement('div');
    peoplesDiv.className = 'peoples-div';

    let buttonsDiv = document.createElement('div');
    buttonsDiv.className = 'buttons-div';

    let addButton = document.createElement('button');
    addButton.className = 'btn add';
    addButton.innerHTML = 'Добавить человечка';
    buttonsDiv.append(addButton);

    let deleteButton = document.createElement('button');
    deleteButton.className = 'btn del';
    deleteButton.innerHTML = 'Удалить человечка';
    buttonsDiv.append(deleteButton);
    wrapperPeoples.append(buttonsDiv);

    for (let people in peoplesArray) {

        let peopleElement = document.createElement('i');
        peopleElement.className = 'fas fa-male';
        peopleElement.setAttribute('data-id', peoplesArray[people].id);
        peopleElement.setAttribute('data-name', peoplesArray[people].name);
        peopleElement.setAttribute('style', 'color: ' + peoplesArray[people].color);

        peopleElement.addEventListener('click', (e) => {
            clickedByPeople(peopleElement);
            getPeopleDomById(peoplesArray[people]);
            getPeopleIdByDom(peoplesArray[people]);
        });

        peoplesDiv.append(peopleElement);
        wrapperPeoples.append(peoplesDiv);

        peoplesArray[people].domElement = peopleElement;

    }

    addButton.addEventListener('click', () => {
        createPeopleInit(CountOfPeople);
        addPeopleInit(peoples, CountOfPeople, peoplesDiv);

    });

    deleteButton.addEventListener('click', () => {
        deletePeopleInit(peoples, peoplesDiv);

    });
};

//Функция, которая создает одного человечка
let createPeopleInit = (peopleCount) => {
    peoples[peopleCount] = {
        id: peopleCount,
        name: generateName(),
        color: getRandomColor()
    };
};

//Функция, которая добавляет одного человечка
let addPeopleInit = (peoplesObject, peopleCount, peoplesDiv) => {
    let peopleElement = document.createElement('i');

    peopleElement.className = 'fas fa-male';
    peopleElement.setAttribute('data-id', peoplesObject[peopleCount].id);
    peopleElement.setAttribute('data-name', peoplesObject[peopleCount].color);
    peopleElement.setAttribute('style', 'color: ' + peoplesObject[peopleCount].color);

    peopleElement.addEventListener('click', (e) => {
        clickedByPeople(peopleElement);
        getPeopleDomById(peoplesObject[peopleCount]);
        getPeopleIdByDom(peoplesObject[peopleCount]);
    });

    peoplesDiv.appendChild(peopleElement);

    peoplesObject[peopleCount].domElement = peopleElement;

};

// Функция, которая удаляет одного человечка
let deletePeopleInit = (peoplesObject, peoplesDiv) => {
    let peoplesDivChildNodes = peoplesDiv.childNodes;
    peoplesDivChildNodes[peoplesDiv.childNodes.length - 1].remove();
};

// Изменение цвета человечка
let changePeopleColor = (elementForChange, newColor) => {
    elementForChange.style.color = newColor;
};

// функция получения DomElement из Id
let getPeopleDomById = (peopleId) => {
    console.log(peopleId.domElement);
    return peopleId.domElement;
};

// функция получения Id из DomElement
let getPeopleIdByDom = (domElement) => {
    console.log(domElement.id);
    return domElement.id;
};

// Функция, которая срабатывает при клике на человечка
let clickedByPeople = (peopleElement) => {
    let newRandomColor = getRandomColor();
    changePeopleColor(peopleElement, newRandomColor);
};

// Функция, которая срабатывает при загрузке структуры документа (когда доступны
// DOM элементы
let domContentLoaded = () => {
    createPeoples(startPeopleCount);
    createPeopleElement(peoples);
};

document.addEventListener('DOMContentLoaded', () => {
    
    domContentLoaded();
    addDivButton.on('click', () => {
        domContentLoaded();
    });

});