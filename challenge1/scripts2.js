const number1 = document.querySelector('#number1');
const number2 = document.querySelector('#number2');
const number3 = document.querySelector('#number3');
const calcButton = document.querySelector('.btn-calc');
let messageBox = document.querySelector('.messageBox');

calcButton.onclick = function() {
    let num1 = parseInt(number1.value);
    let num2 = parseInt(number2.value);
    let num3 = parseInt(number3.value);
    let averageNumber;

    if (num1 == num2 || 
        num1 == num3 || 
        num2 == num3) {
        alert("Вы ввели равные числа!")
        return;
    }

    if (num1 > num2) {

        if (num1 > num3) {

            if (num2 > num3) {
                averageNumber = num2;
            } else {
                averageNumber = num3;
            }
        } else {
            averageNumber = num1;
        }

    } else if (num2 < num3) {
            averageNumber = num2;

        }else if (num1 < num3) {
                averageNumber = num3;
            } 
            else {
                averageNumber = num1;
            }
       
    let messageText = `Среднее число из чисел: 
                        ${num1},  ${num2},  ${num3} является  
                        ${averageNumber} `;
    messageBox.classList.add('show');
    messageBox.innerHTML = messageText;
};